import React, { useEffect, useState } from 'react'
import image from '../images/abs4.jpg'
import './Acceuil.css'

function Acceuil() {
  const imageBackStyle={
    backgroundImage:`linear-gradient(to top,rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.4)),
    url(${image})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat", 
    backgroundSize: "cover"
  }
  const [show,setShow]=useState(false)
  useEffect(()=>{
    const delay=setTimeout(()=>{
      setShow(true);
    },100);
    return ()=>clearTimeout(delay);
  },[]);
  return (
    <div className='container text-container' style={imageBackStyle}>
      
      <section id='accSection' className={show ? 'text-reveal show' : 'text-reveal'}>
      <p id='p1'>Une application pour gérer </p>
      <p id='p2'>les <span>absences</span> de vos étudiants, stagiaires.</p>
      </section>
      
    </div>
  )
}

export default Acceuil