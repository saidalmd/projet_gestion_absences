import React from 'react'
import Cruds from '../components/cruds/Cruds'
import FormUpdateStag from '../components/formUpdateStag/FormUpdateStag'

function ModifierStagiaire() {
  return (
    <div className='container'>
        <Cruds entite="stagiaires"/>
        <FormUpdateStag/>
    </div>
  )
}

export default ModifierStagiaire