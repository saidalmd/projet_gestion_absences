import React from 'react'
import Cruds from '../components/cruds/Cruds'
import TableStagiaires from '../components/tableStagiares/TableStagiaires'
import RechercheStagiaire from '../components/rechercheStagiaire/RechercheStagiaire'

function ConsulterStagiaires() {
  return (
    <div className='container'>
        <Cruds entite="stagiaires"/>
        <RechercheStagiaire/>
        <TableStagiaires/>
    </div>
  )
}

export default ConsulterStagiaires