import React from 'react'
import RecapAbs from '../components/recapAbs/RecapAbs'
import Cruds from '../components/cruds/Cruds'

function RecapAbsences() {
  return (
    <div className='container'>
        <Cruds entite="absences"/>
        <RecapAbs/>
    </div>
  )
}

export default RecapAbsences