import React from 'react'
import Cruds from '../components/cruds/Cruds'
import FormAddStag from '../components/formAddStag/FormAddStag'

function AjouterStagiaire() {
  return (
    <div className='container'>
        <Cruds entite="stagiaires"/>
        <FormAddStag/>
    </div>
  )
}

export default AjouterStagiaire