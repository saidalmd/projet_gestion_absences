import React from 'react'
import Cruds from '../components/cruds/Cruds'
import FormUpdateAbs from '../components/formUpdateAbs/FormUpdateAbs'

function ModifierAbsence() {
  return (
    <div className='container'>
        <Cruds entite="absences"/>
        <FormUpdateAbs/>
    </div>
  )
}

export default ModifierAbsence