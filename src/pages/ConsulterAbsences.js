import React from 'react'
import Cruds from '../components/cruds/Cruds'
import TableAbsences from '../components/tableAbsences/TableAbsences'
import RechercheAbsence from '../components/rechercheAbsence/RechercheAbsence'

function ConsulterAbsences() {
  return (
    <div className='container'>
        <Cruds entite="absences"/>
        <RechercheAbsence/>
        <TableAbsences/>
    </div>
  )
}

export default ConsulterAbsences