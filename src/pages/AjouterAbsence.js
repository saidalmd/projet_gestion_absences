import React from 'react'
import Cruds from '../components/cruds/Cruds'
import FormAddAbs from '../components/formAddAbs/FormAddAbs'

function AjouterAbsence() {
  return (
    <div className='container'>
        <Cruds entite="absences"/>
        <FormAddAbs/>
    </div>
  )
}

export default AjouterAbsence