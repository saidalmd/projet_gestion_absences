const initilaState={
    absences:[],
    dateAbs:"",
    stagAbs:"",
    heurD:"",
    heurF:"",
    duree:"",
    cause:"",
    error:"",
    rechDatePrecise:null,
    rechDateDebut:null,
    rechDateFin:null,
    rechStagId:"",
    rechCause:null
}

const absReducer=(state=initilaState,action)=>{
    switch(action.type){
        case "changeDateAbs":
            return {...state,dateAbs:action.payload}
        case "changeStagAbs":
            return {...state,stagAbs:action.payload}
        case "changeHeurD":
            return {...state,heurD:action.payload}
        case "changeHeurF":
            return {...state,heurF:action.payload}
        case "changeDuree":
            return {...state,duree:action.payload}
        case "changeCause":
            return {...state,cause:action.payload}
        case "changeAbsences":
            return {...state,absences:action.payload}
        case "changeAbsRech":
            let listAbsence=action.payload
            console.log(listAbsence)
            if(state.rechDatePrecise!=null){
                listAbsence=action.payload.filter(element=>element.dateAbs==state.rechDatePrecise)
            }
            if(state.rechDateDebut!=null){
                listAbsence=listAbsence.filter(element=>element.dateAbs>state.rechDateDebut)
            }
            if(state.rechDateFin!=null){
                listAbsence=listAbsence.filter(element=>element.dateAbs<state.rechDateFin)
            }
            if(state.rechStagId!=""){
                console.log(state.rechStagId)
                listAbsence=listAbsence.filter(element=>element.stagAbs==state.rechStagId)
            }
            if(state.rechCause!=null){
                listAbsence=listAbsence.filter(element=>element.cause==state.rechCause)
            }
            console.log("hello from changeAbs Rech")
            console.log(listAbsence)
            return {...state,absences:listAbsence}
        case "changeError":
            return {...state,error:action.payload}
        case "changeRechDatePrecise":
            return {...state,rechDatePrecise:action.payload}
        case "changeRechDateDebut":
            return {...state,rechDateDebut:action.payload}
        case "changeRechDateFin":
            return {...state,rechDateFin:action.payload}
        case "changeRechStagId":
            return {...state,rechStagId:action.payload}
        case "changeRechCause":
            return {...state,rechCause:action.payload}
    }
    return state
}

export default absReducer