export const changeDateAbs=(argument)=>{
    return {type:"changeDateAbs",payload:argument}
}

export const changeStagAbs=(argument)=>{
    return {type:"changeStagAbs",payload:argument}
}

export const changeHeurD=(argument)=>{
    return {type:"changeHeurD",payload:argument}
}

export const changeHeurF=(argument)=>{
    return {type:"changeHeurF",payload:argument}
}

export const changeDuree=(argument)=>{
    return {type:"changeDuree",payload:argument}
}

export const changeCause=(argument)=>{
    return {type:"changeCause",payload:argument}
}

export const changeAbsences=(argument)=>{
    return {type:"changeAbsences",payload:argument}
}

export const changeError=(argument)=>{
    return {type:"changeError",payload:argument}
}

export const changeRechDatePrecise=(argument)=>{
    return {type:"changeRechDatePrecise",payload:argument}
}

export const changeRechDateDebut=(argument)=>{
    return {type:"changeRechDateDebut",payload:argument}
}

export const changeRechDateFin=(argument)=>{
    return {type:"changeRechDateFin",payload:argument}
}

export const changeRechStagId=(argument)=>{
    return {type:"changeRechStagId",payload:argument}
}

export const changeRechCause=(argument)=>{
    return {type:"changeRechCause",payload:argument}
}

export const changeAbsRech=(argument)=>{
    return {type:"changeAbsRech",payload:argument}
}
