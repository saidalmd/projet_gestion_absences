import { act } from "react-dom/test-utils";

const initialState={
    stagiaires:[],
    stagiairesRech:[],
    nom:"",
    prenom:"",
    tele:"",
    error:"",
    rechNom:null,
    rechId:null
}

const stagReducer=(state=initialState,action)=>{
    switch(action.type){
        case "changeId":
            return {...state,id:action.payload};
        case "changeNom":
            return {...state,nom:action.payload}
        case "changePrenom":
            return {...state,prenom:action.payload}
        case "changeTele":
            return {...state,tele:action.payload}
        case "changeStagiairesRech":
            return {...state,stagiairesRech:action.payload}
        case "changeStagiaires":
            let listStag=action.payload
            if(state.rechNom!=null){
                listStag=listStag.filter(ele=>ele.nom.includes(state.rechNom.toLowerCase()))
            }
            if(state.rechId!=null){
                listStag=listStag.filter(ele=>ele.id==state.rechId)
            }
            return {...state,stagiaires:listStag}
        case "changeRechNom":
            return {...state,rechNom:action.payload}
        case "changeRechId":
            return {...state,rechId:action.payload}
        case "changeError":
            return {...state,error:action.payload}
    }
    return state
}

export default stagReducer