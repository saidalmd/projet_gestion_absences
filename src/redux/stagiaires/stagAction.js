export const changeId=(argument)=>{
    return {type:"changeId",payload:argument}
}

export const changeNom=(argument)=>{
    return {type:"changeNom",payload:argument}
}

export const changePrenom=(argument)=>{
    return {type:"changePrenom",payload:argument}
}

export const changeTele=(argument)=>{
    return {type:"changeTele",payload:argument}
}

export const changeStagiaires=(argument)=>{
    return {type:"changeStagiaires",payload:argument}
}

export const changeStagiairesRech=(argument)=>{
    return {type:"changeStagiairesRech",payload:argument}
}

export const changeError=(argument)=>{
    return {type:"changeError",payload:argument}
}

export const changeRechNom=(argument)=>{
    return {type:"changeRechNom",payload:argument}
}

export const changeRechId=(argument)=>{
    return {type:"changeRechId",payload:argument}
}


