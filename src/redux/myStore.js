import { legacy_createStore } from "redux";
import absReducer from "./absences/absReducer";
import stagReducer from "./stagiaires/stagReducer";
import { combineReducers } from "redux";


export const myStore=legacy_createStore(combineReducers({stagReducer,absReducer}))