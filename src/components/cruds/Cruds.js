import React from 'react'
import { Link } from 'react-router-dom';
import './Cruds.css';

function Cruds(props) {
    const {entite}=props;
    let directionConsulter=""
    let directionAjouter=""
    // let directionModifier=""
    if(entite=="stagiaires"){
        directionConsulter="/stagiaires"
        directionAjouter="/stagiaires/ajouter"
        // directionModifier=`/stagiaires/modifier/:idStagiaire`
    }
    else if (entite=="absences") {
        directionConsulter="/absences"
        directionAjouter="/absences/ajouter"
        // directionRecap="/absences/recap"
        // directionModifier="/absences/modifier"
    } 
  return (
    <div id='cruds'>
        <Link to={directionConsulter}>Consulter</Link>
        <Link to={directionAjouter}>Ajouter</Link>
        {entite=="absences" && <Link to="/absences/recap">Recapitulatif</Link> }
    </div>
  )
}

export default Cruds