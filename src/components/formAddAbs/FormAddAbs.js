import React, { useState } from 'react';
import { changeDateAbs,changeStagAbs,changeHeurD,changeHeurF,changeDuree,changeCause,changeError, changeAbsences } from '../../redux/absences/absAction';
import { changeStagiaires } from '../../redux/stagiaires/stagAction';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function FormAddAbs() {
  const [minHeurF,setMinHeurF]=useState(8);
  const [maxDate,setMaxDate]=useState(new Date().toISOString().split("T")[0])

  const navigate=useNavigate()
  const dispatch=useDispatch()

  const stagiaires=useSelector(data=>data.stagReducer.stagiaires)

  const absences=useSelector(data=>data.absReducer.absences)
  const dateAbs=useSelector(data=>data.absReducer.dateAbs)
  const stagAbs=useSelector(data=>data.absReducer.stagAbs)
  const duree=useSelector(data=>data.absReducer.duree)
  const cause=useSelector(data=>data.absReducer.cause)
  const heurD=useSelector(data=>data.absReducer.heurD)
  const heurF=useSelector(data=>data.absReducer.heurF)
  const error=useSelector(data=>data.absReducer.error)

  useEffect(()=>{
    axios.get("http://localhost:8000/stagiaires")
        .then(response=>dispatch(changeStagiaires(response.data))) /*pour le champ select */
    axios.get("http://localhost:8000/absences")
        .then(response=>dispatch(changeAbsences(response.data))) /*pour verifier si cet absence existe deja*/
  },[])

  useEffect(()=>{
    dispatch(changeDuree(parseInt(heurF)-parseInt(heurD)))
  },[heurF,heurD])
  
  const ajouterAbs=(e)=>{ 
    e.preventDefault()
    let listDate=absences.map(ele=>ele.dateAbs)
    let listStag=absences.map(ele=>ele.stagAbs)
    let listHeurD=absences.map(ele=>ele.heureDepart)
    let listHeurF=absences.map(ele=>ele.heureFin)

    if(dateAbs=="" || stagAbs=="" || heurD=="" || heurF=="" || cause==""){
      dispatch(changeError("Veuillez remplir tous les champs"))
    }
    else if(listDate.includes(dateAbs) && listStag.includes(stagAbs) && listHeurD.includes(heurD) && listHeurF.includes(heurF)){
      dispatch(changeError("Attention! Cet absence existe déjà dans la table"))
    }
    else {
    axios.post("http://localhost:8000/absences",{
      "dateAbs":dateAbs,
      "stagAbs":stagAbs,
      "heureDepart":heurD,
      "heureFin":heurF,
      "duree":duree,
      "cause":cause
    })
    navigate("/absences")
  }
  }
  return (
    <form>
      <div>
        <label htmlFor='dateAbs'>Date:</label>
        <input type='date' max={maxDate} name='dateAbs' onChange={(e)=>{dispatch(changeDateAbs(e.target.value))}}/>
      </div>
      <div>
        <label htmlFor='stagAbs'>Stagiaire:</label>
        <select name='stagAbs' onChange={(e)=>{dispatch(changeStagAbs(e.target.value))}}>
          <option value="">Choisir...</option>
          {
            stagiaires.map(element=>{
              return(
                <option value={element.id} key={element.id}>{`${element.nom} ${element.prenom}`}</option>
              )
            })
          }
        </select>
      </div>
      <div>
        <label htmlFor='heurD'>De:</label>
        <input type='number' min={8} max={18} step={1} style={{width:"20%"}} 
        onChange={(e)=>{
          parseInt(e.target.value)<18 ? setMinHeurF(parseInt(e.target.value)+1) : setMinHeurF(18)
          dispatch(changeHeurD(e.target.value))}}/>
        
        <label htmlFor='heurF' style={{width:"20%",marginLeft:"5px"}}>A:</label>
        <input type='number' min={minHeurF} max={18} step={1} style={{width:"20%"}} 
        onChange={(e)=>{
          dispatch(changeHeurF(e.target.value))
          }}/>
      </div>
      <div>
        <span>Cause</span>
        <div id='inputsRadio'>
        <input type='radio' name='cause' value="justifie" onChange={(e)=>{dispatch(changeCause(e.target.value))}}/>
        <label>Justifié</label>
        <input type='radio' name='cause' value="non_justifie" onChange={(e)=>{dispatch(changeCause(e.target.value))}}/>
        <label>Non justifié</label>
        </div>
      </div>
      <div>
        <input type='submit' value="Ajouter" onClick={ajouterAbs}/>
      </div>
      <span id='error'>{error}</span>
    </form>
  )
}

export default FormAddAbs