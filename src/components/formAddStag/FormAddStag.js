import React,{useEffect, useState} from 'react';
import axios from 'axios';
import { changeNom,changePrenom,changeTele,changeError, changeStagiaires } from '../../redux/stagiaires/stagAction';
import { useSelector,useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import './FormAddStag.css';

function FormAddStag() {
  const [length,setLength]=useState("")

  const dispatch=useDispatch()

  const stagiaires=useSelector(data=>data.stagReducer.stagiaires)
  const nom=useSelector(data=>data.stagReducer.nom)
  const prenom=useSelector(data=>data.stagReducer.prenom)
  const tele=useSelector(data=>data.stagReducer.tele)
  const error=useSelector(data=>data.stagReducer.error)

  const navigate=useNavigate()

  useEffect(()=>{
    axios.get("http://localhost:8000/stagiaires")
        .then(response=>dispatch(changeStagiaires(response.data)))
    axios.get("http://localhost:8000/lengthStag")
        .then(response=>setLength(response.data[0].length))
  },[])

  

  const ajouterStag=(e)=>{
    e.preventDefault()
    let listNom=stagiaires.map(ele=>ele.nom)
    let listPrenom=stagiaires.map(ele=>ele.prenom)
    let listTele=stagiaires.map(ele=>ele.tele)
    if(nom=="" || prenom=="" || tele==""){
      dispatch(changeError("Veuillez saisir tous les champs"))
    }
    else if(tele.length!=10){
      dispatch(changeError("Veuillez saisir un numero de telephone valide"))
    }
    else if(listNom.includes(nom) && listPrenom.includes(prenom) && listTele.includes(tele) ){
      dispatch(changeError("Ce nom et prenom existent dans la base de données! Différenciez les"))
    }
    else {
    axios.post("http://localhost:8000/stagiaires",{
      "id":(parseInt(length)+1).toString(),
      "nom":nom,
      "prenom":prenom,
      "tele":tele
    })
    axios.put("http://localhost:8000/lengthStag/1",{
      "length":(parseInt(length)+1).toString()
    })
    navigate("/stagiaires")
  }
  }
  return (
    <form>
      {/* <p>Formulaire ajouter stagiaire</p> */}
      <div>
        <label htmlFor='nom'>Nom:</label>
        <input type='text' name='nom' id="nom" onChange={(e)=>{dispatch(changeNom(e.target.value))}} required/>
      </div>
      <div>
        <label htmlFor='prenom'>Prenom:</label>
        <input type='text' name='prenom' id="prenom" onChange={(e)=>{dispatch(changePrenom(e.target.value))}} required/>
      </div>
      <div>
        <label htmlFor='tele'>Telephone:</label>
        <input type='text' name='tele' id="tele" onChange={(e)=>{dispatch(changeTele(e.target.value))}} required/>
      </div>
      <div>
        <input type='submit' value="Ajouter" onClick={ajouterStag}/>
      </div>
      <span id='error'>{error}</span>
    </form>
  )
}

export default FormAddStag