import React, { useEffect } from 'react';
import axios from 'axios';
import { useSelector,useDispatch } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { changeNom,changePrenom,changeTele,changeError, changeStagiaires } from '../../redux/stagiaires/stagAction';

function FormUpdateStag() {
  const {idStagiaire}=useParams()
  const nom=useSelector(data=>data.stagReducer.nom)
  const prenom=useSelector(data=>data.stagReducer.prenom)
  const tele=useSelector(data=>data.stagReducer.tele)
  const error=useSelector(data=>data.stagReducer.error)
  const stagiaires=useSelector(data=>data.stagReducer.stagiaires)

  const dispatch=useDispatch()
  const navigate=useNavigate()
  
  useEffect(()=>{
    axios.get("http://localhost:8000/stagiaires")
        .then(response=>dispatch(changeStagiaires(response.data)))
    axios.get(`http://localhost:8000/stagiaires/${idStagiaire}`)
        .then(response=>{
          dispatch(changeNom(response.data.nom))
          dispatch(changePrenom(response.data.prenom))
          dispatch(changeTele(response.data.tele))
        })
        .catch(error=>console.log(error))
  },[])

  const modifier=(e)=>{
    e.preventDefault()
    let listNom=stagiaires.map(ele=>ele.nom)
    let listPrenom=stagiaires.map(ele=>ele.prenom)
    let listTele=stagiaires.map(ele=>ele.tele)
    if(nom=="" || prenom=="" || tele==""){
      dispatch(changeError("Veuillez saisir tous les champs"))
    }
    else if(tele.length!=10){
      dispatch(changeError("Veuillez saisir un numero de telephone valide"))
    }
    else if(listNom.includes(nom) && listPrenom.includes(prenom) && listTele.includes(tele) ){
      dispatch(changeError("Ce nom et prenom existent dans la base de données! Différenciez les"))
    }
    else {
    axios.put(`http://localhost:8000/stagiaires/${idStagiaire}`,{
      "id":idStagiaire,
      "nom":nom,
      "prenom":prenom,
      "tele":tele
    })
    navigate('/stagiaires')
  }
    
  }
  return (
    <form>
      {/* <p>Formulaire modifier stagiaire</p> */}
      <div>
        <label htmlFor='id'>ID:</label>
        <input type='number' name='id' id="id" value={idStagiaire}  readOnly/>
      </div>
      <div>
        <label htmlFor='nom'>Nom:</label>
        <input type='text' name='nom' id="nom"  onChange={(e)=>dispatch(changeNom(e.target.value))} value={nom} />
      </div>
      <div>
        <label htmlFor='prenom'>Prenom:</label>
        <input type='text' name='prenom' id="prenom" value={prenom} onChange={(e)=>dispatch(changePrenom(e.target.value))}/>
      </div>
      <div>
        <label htmlFor='tele'>Telephone:</label>
        <input type='text' name='tele' id="tele" value={tele} onChange={(e)=>dispatch(changeTele(e.target.value))}/>
      </div>
      <div>
        <input type='submit' value="Modifier" onClick={modifier}/>
      </div>
      <span id='error'>{error}</span>
        
    </form>
  )
}

export default FormUpdateStag