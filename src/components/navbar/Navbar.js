import React from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';

function Navbar() {
  return (
    <nav>
        <div>
            <span></span>
        </div>
        <div>
            <Link to='/'>Acceuil</Link>
            <Link to='/stagiaires'>Gestion stagiaires</Link>
            <Link to='/absences'>Gestion absences</Link>
        </div>
    </nav>
  )
}

export default Navbar