import React, { useEffect } from 'react'
import { changeRechNom,changeRechId,changeStagiaires,changeStagiairesRech } from '../../redux/stagiaires/stagAction'
import styles from '../rechercheAbsence/RechercheAbsence.module.css'
import { useDispatch,useSelector } from 'react-redux'
import axios from 'axios'


function RechercheStagiaire() {
  const dispatch=useDispatch()
  const stagiaires=useSelector(data=>data.stagReducer.stagiaires)
  const stagiairesRech=useSelector(data=>data.stagReducer.stagiairesRech)

  useEffect(()=>{
    axios.get("http://localhost:8000/stagiaires")
        .then(response=>dispatch(changeStagiairesRech(response.data)))
  },[])

  const changeStag=()=>{
    axios.get("http://localhost:8000/stagiaires")
        .then(response=>dispatch(changeStagiaires(response.data)))
  }
  return (
    <form onSubmit={(e)=>e.preventDefault()} className={styles.form} >
        <section className={styles.section1}>
            <div>
                <label htmlFor='nom'>Stagiaire</label>
                <input type='text' onChange={(e)=>{
                    dispatch(changeRechNom(e.target.value))
                    changeStag()
                }}/>
                {/* <select id='nom' name='nom' onChange={(e)=>{dispatch(changeRechNom(e.target.value))}}>
                    <option value="">Choisir...</option>
                    {
                        stagiairesRech.map(element=>{
                            return(
                                <option value={element.id} key={element.id}>{`${element.nom} ${element.prenom}`}</option>
                            )
                        })
                    }
                </select> */}
            </div>
        </section>
        <section className={styles.section2}>
            <input type='submit' value="Rechercher" onClick={changeStag} className={`${styles.btnSubmit} ${styles.btn}`}/>
            <input type='reset' value="Reset" className={`${styles.btnReset} ${styles.btn}`}/>
        </section>

    </form>
  )
}

export default RechercheStagiaire