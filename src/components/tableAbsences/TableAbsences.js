import React, { useEffect } from 'react';
import axios from 'axios';
import { changeAbsences } from '../../redux/absences/absAction';
import { useSelector,useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';


function TableAbsences() {
  const dispatch=useDispatch()
  const absences=useSelector(data=>data.absReducer.absences)
  const stagiaires=useSelector(data=>data.stagReducer.stagiaires)

  useEffect(()=>{
    axios.get("http://localhost:8000/absences")
        .then(response=>dispatch(changeAbsences(response.data)))
  },[absences])

  const supprimer=(argument)=>{
    if(window.confirm(`Voulez-vous supprimer cet enregistrement?`)){
      console.log(argument)
      axios.delete(`http://localhost:8000/absences/${argument}`)
    }
  }
  
  return (
    <>
    {
      absences.length>0 ? (
        <table>
      <thead>
        <tr>
          <th>Date absence</th>
          <th>Stagiaire</th>
          <th>Duree</th>
          <th>Cause</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {
          
          absences.map(element=>{
            let stag=stagiaires.filter(stag=>{
              return stag.id==element.stagAbs
            })
            let nomStag=`${stag[0].nom} ${stag[0].prenom}`
            return(
              <tr key={element.id}>
                <td>{element.dateAbs}</td>
                <td>{nomStag}</td>
                <td>{`${element.heureDepart}h-${element.heureFin}h`}</td>
                <td>{element.cause=="non_justifie"?"Non justifié":"Justifié"}</td>
                <td>
                  <Link to={`/absences/modifier/${element.id}`}>Modifier</Link>
                  <button onClick={()=>supprimer(element.id)}>Supprimer</button>
                </td>
              </tr>
            )
          })
        }
      </tbody>
        
    </table>

      ):(
        <p id='pNull'>
          Aucun enregistrement ne correspond à votre recherche
        </p>
      )
    }
    </>
  )
}

export default TableAbsences