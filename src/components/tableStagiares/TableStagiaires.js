import React, { useEffect } from 'react';
import axios from 'axios';
import { changeStagiaires } from '../../redux/stagiaires/stagAction';
import { useSelector,useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import './TableStagiaires.css';
import { changeAbsences } from '../../redux/absences/absAction';

function TableStagiaires() {
  const dispatch=useDispatch()
  const stagiaires=useSelector(data=>data.stagReducer.stagiaires)
  const absences=useSelector(data=>data.absReducer.absences)

  useEffect(()=>{
    axios.get("http://localhost:8000/stagiaires")
        .then(response=>dispatch(changeStagiaires(response.data)))
    axios.get("http://localhost:8000/absences")
        .then(response=>dispatch(changeAbsences(response.data)))
  },[stagiaires])

  const supprimer=(argument)=>{
    console.log("hello")
    if(window.confirm("Voulez vous supprimer ce stagiaire?")){
      let absencesStag=absences.filter(ele=>ele.stagAbs==argument)
      axios.delete(`http://localhost:8000/stagiaires/${argument}`)
      absencesStag.map(ele=>axios.delete(`http://localhost:8000/absences/${ele.id}`))

    }
  }

  return (
    <>
    {
      stagiaires.length>0?(
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Nom</th>
          <th>Prenom</th>
          <th>Telephone</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {
          stagiaires.map(element=>{
            return(
              <tr key={element.id}>
                <td>{element.id}</td>
                <td>{element.nom}</td>
                <td>{element.prenom}</td>
                <td>{element.tele}</td>
                <td>
                  <Link to={`/stagiaires/modifier/${element.id}`}>Modifier</Link>
                  <button onClick={()=>supprimer(element.id)}>Supprimer</button>
                </td>
              </tr>
            )
          })
        }
      </tbody>
    </table>):(
        <p id='pNull'>
          Aucun enregistrement ne correspond à votre recherche
        </p>
      )
    }
    </>
  )
      
}

export default TableStagiaires