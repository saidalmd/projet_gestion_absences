import React, { useEffect,useState,useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { changeCause, changeDateAbs, changeDuree, changeStagAbs,changeHeurD,changeHeurF, changeAbsences,changeError } from '../../redux/absences/absAction'
import { useNavigate, useParams } from 'react-router-dom'
import axios from 'axios'

function FormUpdateAbs() {
  const navigate=useNavigate()
  const {idAbsence}=useParams()

  const [minHeurF,setMinHeurF]=useState(8);

  const checkJust=useRef(null)
  const checkNonJust=useRef(null)

  const dispatch=useDispatch()
  const stagiaires=useSelector(data=>data.stagReducer.stagiaires) /*pour afficher les stagiaires dans select */

  const dateAbs=useSelector(data=>data.absReducer.dateAbs)
  const stagAbs=useSelector(data=>data.absReducer.stagAbs)
  const heurD=useSelector(data=>data.absReducer.heurD)
  const heurF=useSelector(data=>data.absReducer.heurF)
  const duree=useSelector(data=>data.absReducer.duree)
  const cause=useSelector(data=>data.absReducer.cause)
  const absences=useSelector(data=>data.absReducer.absences)
  const error=useSelector(data=>data.absReducer.error)

  useEffect(()=>{
    axios.get("http://localhost:8000/absences")
        .then(response=>dispatch(changeAbsences(response.data)))
    axios.get(`http://localhost:8000/absences/${idAbsence}`)
        .then(response=>{
          dispatch(changeDateAbs(response.data.dateAbs))
          dispatch(changeStagAbs(response.data.stagAbs))
          dispatch(changeHeurD(response.data.heureDepart))
          dispatch(changeHeurF(response.data.heureFin))
          dispatch(changeDuree(response.data.duree))
          dispatch(changeCause(response.data.cause))
          if(response.data.cause=="justifie"){
            if(checkJust.current){
              checkJust.current.checked=true
              checkNonJust.current.checked=false
            }
          }
          else {
            if(checkNonJust.current){
              checkJust.current.checked=false
              checkNonJust.current.checked=true
            }
          }
        })
  },[])

  useEffect(()=>{
    dispatch(changeDuree(parseInt(heurF)-parseInt(heurD)))
  },[heurF,heurD])

  const modifier=(e)=>{
    e.preventDefault()
    let listDate=absences.map(ele=>ele.dateAbs)
    let listStag=absences.map(ele=>ele.stagAbs)
    let listHeurD=absences.map(ele=>ele.heureDepart)
    let listHeurF=absences.map(ele=>ele.heureFin)
    if(dateAbs=="" || stagAbs=="" || heurD=="" || heurF=="" || cause==""){
      dispatch(changeError("Veuillez remplir tous les champs"))
    }
    else if(listDate.includes(dateAbs) && listStag.includes(stagAbs) && listHeurD.includes(heurD) && listHeurF.includes(heurF)){
      dispatch(changeError("Attention! Cet absence existe déjà dans la table"))
    }
    else {
    axios.put(`http://localhost:8000/absences/${idAbsence}`,{
      "dateAbs":dateAbs,
      "stagAbs":stagAbs,
      "heureDepart":heurD,
      "heureFin":heurF,
      "duree":duree,
      "cause":cause
    })
    navigate('/absences')
  }
  }
  return (
    <form>
      <div>
        <label htmlFor='dateAbs'>Date:</label>
        <input type='date' name='dateAbs' value={dateAbs} onChange={(e)=>dispatch(changeDateAbs(e.target.value))}/>
      </div>
      <div>
        <label htmlFor='stagAbs'>Stagiaire:</label>
        <select name='stagAbs' value={stagAbs} onChange={(e)=>dispatch(changeStagAbs(e.target.value))}>
          <option value="">Choisir...</option>
          {
            stagiaires.map(element=>{
              return(
                <option value={element.id} key={element.id}>{`${element.nom} ${element.prenom}`}</option>
              )
            })
          }
        </select>
      </div>
      <div>
        <label htmlFor='heurD'>De:</label>
        <input type='number' value={heurD} min={8} max={18} step={1} style={{width:"20%"}} 
        onChange={(e)=>{
          parseInt(e.target.value)<18 ? setMinHeurF(parseInt(e.target.value)+1) : setMinHeurF(18)
          dispatch(changeHeurD(e.target.value))}}/>
        
        <label htmlFor='heurF' style={{width:"20%",marginLeft:"5px"}}>A:</label>
        <input type='number' value={heurF} min={minHeurF} max={18} step={1} style={{width:"20%"}} 
        onChange={(e)=>{
          dispatch(changeHeurF(e.target.value))
          }}/>
      </div>
      <div>
        <span>Cause</span>
        <div id='inputsRadio'>
        <input type='radio' name='cause' ref={checkJust}  value="justifie" 
        onChange={(e)=>{
          checkJust.current.removeAttribute("checked")
          checkNonJust.current.removeAttribute("checked")
          dispatch(changeCause(e.target.value))}}/>
        <label>Justifié</label>
        <input type='radio' name='cause' ref={checkNonJust} value="non_justifie" onChange={(e)=>{
          checkNonJust.current.removeAttribute("checked")
          checkJust.current.removeAttribute("checked")
          dispatch(changeCause(e.target.value))}}/>
        <label>Non justifié</label>
        </div>
      </div>
      <div>
        <input type='submit' value="Modifier" onClick={modifier}/>
      </div>
      <span id='error'>{error}</span>
    </form>
  )
}

export default FormUpdateAbs