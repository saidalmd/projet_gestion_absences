import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { changeAbsences } from '../../redux/absences/absAction'
import axios from 'axios'
import { changeStagiaires } from '../../redux/stagiaires/stagAction'
import './RecapAbs.css'

function RecapAbs() {
    const absences=useSelector(data=>data.absReducer.absences)
    const stagiaires=useSelector(data=>data.stagReducer.stagiaires)
    const dispatch=useDispatch()

    useEffect(()=>{
        axios.get("http://localhost:8000/absences")
            .then(response=>dispatch(changeAbsences(response.data)))
        axios.get("http://localhost:8000/stagiaires")
            .then(response=>dispatch(changeStagiaires(response.data)))
    },[])

     let totalAbsJustifies=absences.filter(element=>element.cause=="justifie")
     let totalAbsNonJustifies=absences.filter(element=>element.cause=="non_justifie")
  return (
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Stagiaire</th>
                <th>Nombre d'heures</th>
                {/* <th>Cause</th> */}
            </tr>
        </thead>
        <tbody>
            {
                stagiaires.map(stag=>{
                    let absJustifies=absences.filter(abs=>abs.cause=="justifie" && abs.stagAbs==stag.id)
                    let absNonJustifies=absences.filter(abs=>abs.cause=="non_justifie" && abs.stagAbs==stag.id)
                    return(
                        <tr>
                            <td>{stag.id}</td>
                            <td>{`${stag.nom} ${stag.prenom}`}</td>
                            <td>
                                <ul>
                                    <li><span>Justifiés</span>:{absJustifies.reduce((total,element)=>total+parseFloat(element.duree),0)} </li>
                                    <li><span>Non justifiés</span>:{absNonJustifies.reduce((total,element)=>total+parseFloat(element.duree),0)}</li>
                                </ul>
                            </td>
                        </tr>
                    )
                })
            }
            <tr>
                <th colSpan={2} id='total'>Total</th>
                <td>
                    <ul>
                        <li><span>Justifiés</span>{totalAbsJustifies.reduce((total,element)=>total+parseFloat(element.duree),0)}</li>
                        <li><span>Non justifiés</span>{totalAbsNonJustifies.reduce((total,element)=>total+parseFloat(element.duree),0)}</li>
                    </ul>
                </td>
            </tr>


            {/* <tr>
                <td>
                    {
                        absJustifies.reduce((total,element)=>total+parseFloat(element.duree),0)
                    }
                </td>
                <td>Justifiés</td>
            </tr>
            <tr>
                <td>
                    {
                        absNonJustifies.reduce((total,element)=>total+parseFloat(element.duree),0)
                    }
                </td>
                <td>Non justifiés</td>
            </tr> */}
        </tbody>
        
    </table>
  )
}

export default RecapAbs