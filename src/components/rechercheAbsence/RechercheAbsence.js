import React, { useState } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { useEffect } from 'react';
import axios from 'axios';
import { changeStagiaires } from '../../redux/stagiaires/stagAction';
import { changeRechDateDebut,changeRechDateFin,changeRechStagId,changeRechCause,changeAbsRech,changeRechDatePrecise } from '../../redux/absences/absAction';
import styles from './RechercheAbsence.module.css';
// import './RechercheAbsence.css';

function RechercheAbsence() {
  const [dis,setDis]=useState({
    dateP:false,
    dateD:false,
    dateF:false
  })
  const dispatch=useDispatch()
  const stagiaires=useSelector(data=>data.stagReducer.stagiaires)
  
  useEffect(()=>{
    axios.get("http://localhost:8000/stagiaires")
        .then(response=>dispatch(changeStagiaires(response.data)))
  },[])

  const changeAbs=()=>{
    axios.get("http://localhost:8000/absences")
        .then(response=>dispatch(changeAbsRech(response.data)))
  }
  const resetAbs=()=>{
    setDis({...dis,dateD:false,dateP:false,dateF:false})
    dispatch(changeRechDatePrecise(null))
    dispatch(changeRechDateDebut(null))
    dispatch(changeRechDateFin(null))
    dispatch(changeRechStagId(""))
    dispatch(changeRechCause(null))
    changeAbs()
  }
  
  return (      
    <form onSubmit={(e)=>e.preventDefault()} id="form_rech" className={styles.form} >
        <section className={styles.section1}>
            <div>
                <label htmlFor='datePrecise'>Date:</label>
                <input type='date' disabled={dis.dateP} name='datePrecise' id='datePrecise' 
                onChange={(e)=>{
                    setDis({...dis,dateD:true,dateF:true})
                    dispatch(changeRechDatePrecise(e.target.value))}}/>
            </div>
            <div>
                <label htmlFor='dateDebut' className={styles.widthLabel}>Date début</label>
                <input type='date' disabled={dis.dateD} name='dateDebut' id='dateDebut' 
                onChange={(e)=>{
                    setDis({...dis,dateP:true,dateF:false})
                    dispatch(changeRechDateDebut(e.target.value))}}/>
            </div>
            <div>
                <label htmlFor='dateFin' className={styles.widthLabel}>Date fin</label>
                <input type='date' disabled={dis.dateF} name='dateFin' id='dateFin' onChange={(e)=>{dispatch(changeRechDateFin(e.target.value))}}/>
            </div>
            <div>
                <label htmlFor='stagAbs'>Stagiaire</label>
                <select id='stagAbs' name='stagAbs' onChange={(e)=>{dispatch(changeRechStagId(e.target.value))}}>
                    <option value="">Choisir...</option>
                    {
                        stagiaires.map(element=>{
                            return(
                                <option value={element.id} key={element.id}>{`${element.nom} ${element.prenom}`}</option>
                            )
                        })
                    }
                </select>
            </div>
            <div>
                <label htmlFor='cause'>Cause</label>
                <select id='cause' name='cause' onChange={(e)=>{dispatch(changeRechCause(e.target.value))}}>
                    <option value="all">All</option>
                    <option value="justifie">Justifiés</option>
                    <option value="non_justifie">Non justifiés</option>
                </select>
            </div>
        </section>
        <section className={styles.section2}>
            <input type='submit' value="Rechercher" onClick={changeAbs} className={`${styles.btnSubmit} ${styles.btn}`}/>
            <input type='reset' value="Reset" onClick={resetAbs} className={`${styles.btnReset} ${styles.btn}`}/>
        </section>

    </form>
  )
}

export default RechercheAbsence