import logo from './logo.svg';
import './App.css';
import { BrowserRouter,Routes,Route } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import ConsulterStagiaires from './pages/ConsulterStagiaires';
import ConsulterAbsences from './pages/ConsulterAbsences';
import Acceuil from './pages/Acceuil';
import AjouterStagiaire from './pages/AjouterStagiaire';
import ModifierStagiaire from './pages/ModifierStagiaire';
import AjouterAbsence from './pages/AjouterAbsence';
import ModifierAbsence from './pages/ModifierAbsence';
import RecapAbsences from './pages/RecapAbsences';
import Footer from './components/footer/Footer';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar/>
        <Routes>
          <Route path='/' element={<Acceuil/>}/>
          <Route path='/stagiaires' element={<ConsulterStagiaires/>}/>
          <Route path='/absences' element={<ConsulterAbsences/>}/>
          <Route path='/stagiaires/ajouter' element={<AjouterStagiaire/>}/>
          <Route path='/stagiaires/modifier/:idStagiaire' element={<ModifierStagiaire/>}/>
          <Route path='/absences/ajouter' element={<AjouterAbsence/>}/>
          <Route path='/absences/modifier/:idAbsence' element={<ModifierAbsence/>}/>
          <Route path='/absences/recap' element={<RecapAbsences/>}/>
        </Routes>
        <Footer/>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
